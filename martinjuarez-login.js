{
  const {
    html,
  } = Polymer;
  /**
    `<martinjuarez-login>` Description.

    Example:

    ```html
    <martinjuarez-login></martinjuarez-login>
    ```

    ## Styling
    The following custom properties and mixins are available for styling:

    ### Custom Properties
    | Custom Property     | Selector | CSS Property | Value       |
    | ------------------- | -------- | ------------ | ----------- |
    | --cells-fontDefault | :host    | font-family  |  sans-serif |
    ### @apply
    | Mixins    | Selector | Value |
    | --------- | -------- | ----- |
    | --martinjuarez-login | :host    | {} |

    * @customElement
    * @polymer
    * @extends {Polymer.Element}
    * @demo demo/index.html
  */
  class MartinjuarezLogin extends Polymer.Element {

    static get is() {
      return 'martinjuarez-login';
    }

    static get properties() {
      return {
        prop1: {
          type: String,
          value: 'my-login-martin'
        },

        user1: {
          type: String,
          value: ''
        },

        pass1: {
          type: String,
          value: ''
        },

        acceso: {
          type: Boolean,
          value: false,
          notify: true
        }

      };


    }


    static get template() {
      return html `
      <style include="martinjuarez-login-styles martinjuarez-login-shared-styles"></style>
      <slot></slot>

          <div class="l">
            <br><br>
            <p>Bienvenido</p>
            <br><br>
            <label  for="name">Username:</label ><br>
            <input  value="{{user1::input}}" type="text" name="user1" style="text-align:center">
            <br><br>
            <label for="username">Password: </label><br>
            <input value="{{pass1::input}}" type="password" name="pass1"  style="text-align:center">
            <br><br>
            <button class="boton"  on-click="login">Login</button>
          </div>

      `;
    }


    login(user1, pass1) {
      if (this.pass1 === 'CONTRASENA' && this.user1 === 'USUARIO') {
        alert('Acceso correcto');
        this.dispatchEvent(new CustomEvent('login-success', {detail: this.user1}));
        console.log('acceso correcto');
      } else {
        alert('Por favor ingrese, nombre de usuario y contraseña correctos');
        this.dispatchEvent(new CustomEvent('login-error', {detail: this.user1}));
        console.log('acceso incorrecto');

      }
    }
  }

  customElements.define(MartinjuarezLogin.is, MartinjuarezLogin);
}
